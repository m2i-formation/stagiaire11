package org.m2iformation.dolibarr.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class Connexion {
	
	
	public static ChromeDriver driver;
	
	
	@BeforeClass
	public static void setupClass() {
		//on initialise l'objet driver
	//ChromeDriver driver = new ChromeDriver();
	
	driver = new ChromeDriver();
	
	//on rajoute le Wait si lag
	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	
	
	
	@Before
	public void setup() {
		//lancement � l'adresse
	}

	@Test
	public void testConnexion() throws InterruptedException {
		//les steps et verifications du test
		//1) on se connecte
		driver.get("http://demo.testlogiciel.pro/dolibarr/");
		driver.findElement(By.id("username")).sendKeys("jsmith");
		driver.findElement(By.name("password")).sendKeys("dolibarrhp");
		driver.findElement(By.xpath("//input[contains(@value,'Connexion')]")).click();
		assertTrue(driver.findElement(By.className("titre")).isDisplayed());
	//	assertEquals("Espace accueil",driver.findElement(By.className("titre")).getText());
		assertEquals("Espace accueil",driver.findElement(By.className("titre")).getText());
		
		//on rajoute sleep = wait equivalent selenium
		Thread.sleep(5000);
		
		driver.findElement(By.xpath("//img[@alt='D�connexion']")).click();
	}
	
	@After
	public void teardown() {
		
	}
	
	
	@AfterClass
	public static void tearDownClass() {
		//on quitte le driver
		driver.quit();
	}
	
	
}
